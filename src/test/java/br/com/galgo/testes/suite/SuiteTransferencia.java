package br.com.galgo.testes.suite;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.TesteExpurgo;
import br.com.galgo.TesteTransferencia;

@RunWith(Categories.class)
@IncludeCategory(TesteTransferencia.class)
@Suite.SuiteClasses({ TesteExpurgo.class })
public class SuiteTransferencia {


}
